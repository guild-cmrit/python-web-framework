from flask import Flask, render_template

app = Flask(__name__)

@app.route("/")
def index():
    texts="johnny"
    return render_template("index.html", text=texts)

@app.route("/more")
def more():
    name=["Ram","Raju","Ramesh"]
    return render_template("more.html",names=name)
