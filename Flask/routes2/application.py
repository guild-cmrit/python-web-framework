from flask import Flask,render_template

app = Flask(__name__)

@app.route("/")
def home():
	return "<h1>Hello</h1>"


@app.route("/<string:name>")
def name(name):
	return f"<h1>Hello {name}</h1>"

@app.route("/index")
def index():
    headline = "Hello, world!"
    return render_template("index.html", text=headline)
